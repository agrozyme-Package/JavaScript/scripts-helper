import Shell from '../../Shell';
import Base, {Commands} from '../Base';

class npm extends Base {

  getCommands(): Commands {
    const items = super.getCommands();
    items.addGlobalPackage = 'npm install --global';
    items.getGlobalModulePath = 'npm root --global';
    items.linkGlobalPackage = 'npm link';
    return items;
  }

  getGlobalPackages() {
    const command = 'npm list --global --depth=0 --json';
    let text = Shell.run(command, {}, true);
    text = (false === text) ? '' : text.toString();

    // noinspection UnusedCatchParameterJS
    try {
      const item = JSON.parse(text);
      return item.hasOwnProperty('dependencies') ? item.dependencies : {};
    } catch (error) {
      return {};
    }
  }

}

export default npm;
