'use strict';

const script = () => {
  const {PackageManager, Shell, ObjectInspector} = require('../index');
  const manager = PackageManager.detect();
  const packages = ['webpack', 'webpack-cli', {name: 'ts-loader', link: true}];

  if (false === manager.requireGlobalPackage('typescript')) {
    process.exit(1);
  }

  if (false === manager.requireAllGlobalPackages(packages)) {
    process.exit(1);
  }

  if (false === Shell.run('tsc')) {
    process.exit(1);
  }

  if (false === Shell.run('webpack')) {
    process.exit(1);
  }

  ObjectInspector.dir(packages, {colors: true});

};

script();
