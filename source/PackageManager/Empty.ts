import Base, {Package} from './Base';

class Empty extends Base {
  constructor() {
    super();
    console.log(this.getErrorText());
  }

  addGlobalPackage(item: string): boolean {
    return false;
  }

  getGlobalPackages(): object {
    return {};
  }

  linkGlobalPackage(item: string): boolean {
    return false;
  }

  requireAllGlobalPackages(items: (Package | string)[]): boolean | boolean {
    return false;
  }

  requireGlobalPackage(item: string, module: string = ''): boolean {
    return super.requireGlobalPackage(item, module);
  }

  // noinspection JSMethodCanBeStatic
  protected getErrorText() {
    let text = 'Not support current package manager';

    if (process.stdout.isTTY) {
      const red = '\x1B[31m';
      const normal = '\x1B[0m';
      const error = `${red}error${normal} `;
      text = error + text;
    }

    return text;
  }
}

export default Empty;
