"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const path_1 = require("path");
const Empty_1 = tslib_1.__importDefault(require("./Empty"));
const npm_1 = tslib_1.__importDefault(require("./Registry/npm"));
const pnpm_1 = tslib_1.__importDefault(require("./Registry/pnpm"));
const yarn_1 = tslib_1.__importDefault(require("./Registry/yarn"));
class Factory {
    constructor() {
        this.managers = {
            npm: npm_1.default,
            yarn: yarn_1.default,
            pnpm: pnpm_1.default
        };
    }
    detect() {
        if (undefined === this.item) {
            this.item = this.getManager(this.parseMangerName());
        }
        return this.item;
    }
    getManager(name) {
        const managers = this.managers;
        if (false === managers.hasOwnProperty(name)) {
            return new Empty_1.default();
        }
        const classContructor = managers[name];
        return new classContructor();
    }
    // noinspection JSMethodCanBeStatic
    parseMangerName() {
        const index = 'npm_execpath';
        if (false === process.env.hasOwnProperty(index)) {
            return '';
        }
        const environment = process.env;
        const [name = ''] = path_1.parse(environment[index]).name.split('-');
        return name;
    }
}
Factory.instance = new Factory();
exports.Factory = Factory;
exports.default = Factory.instance;
