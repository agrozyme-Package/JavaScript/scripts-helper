"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
module.exports = {
    mode: 'none',
    target: 'node',
    entry: './index.ts',
    module: {
        rules: [{
                test: /\.tsx?$/,
                use: 'ts-loader'
            }]
    },
    resolve: { extensions: ['.tsx', '.ts', '.js'] },
    output: {
        filename: 'bundle.js',
        path: path_1.resolve(__dirname, '.webpack')
    }
};
