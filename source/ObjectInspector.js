"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("util");
class ObjectInspector {
    constructor() {
    }
    static dir(item, options = ObjectInspector.options) {
        const text = ObjectInspector.wrap(item, options);
        return console.log(text);
    }
    static getInspectFunction(item) {
        if (null === item) {
            return null;
        }
        const custom = util_1.inspect.custom;
        const index = Object.getOwnPropertySymbols(item).findIndex(value => value === custom);
        return (0 > index) ? null : item[custom];
    }
    static wrap(item, options = ObjectInspector.options) {
        const instance = ObjectInspector.instance;
        const bind = instance.bindInspectFunction;
        const oldFunction = bind(item, instance[util_1.inspect.custom]);
        const text = util_1.inspect(item, options);
        bind(item, oldFunction);
        return text;
    }
    // noinspection JSMethodCanBeStatic
    bindInspectFunction(item, inspectFunction) {
        if (('object' !== typeof item) || (null === item)) {
            return null;
        }
        const custom = util_1.inspect.custom;
        const oldFunction = ObjectInspector.getInspectFunction(item);
        if ((null !== inspectFunction) && (null === oldFunction)) {
            item[custom] = inspectFunction.bind(item);
        }
        if ((null === inspectFunction) && (null !== oldFunction)) {
            delete item[custom];
        }
        return oldFunction;
    }
    [util_1.inspect.custom](depth, options) {
        if ((null !== depth) && (0 > depth)) {
            return options.stylize('<MaxDepth>', 'special');
        }
        const newDepth = (null === options.depth) ? null : options.depth - 1;
        const newOptions = Object.assign({}, options, { depth: newDepth });
        const isArray = this instanceof Array;
        const replace = '\n  ';
        const items = [];
        for (let index in this) {
            const item = this[index];
            if ('function' === typeof item) {
                continue;
            }
            const text = ObjectInspector.wrap(item, newOptions).replace(/\n/g, replace);
            items.push(`${index}: ${text}`);
        }
        const text = items.join(`,${replace}`);
        if (isArray) {
            return ('' === text) ? '[]' : `[${replace}${text}\n]`;
        }
        else {
            return ('' === text) ? '{}' : `{${replace}${text}\n}`;
        }
    }
}
ObjectInspector.instance = new ObjectInspector();
ObjectInspector.options = {};
exports.default = ObjectInspector;
